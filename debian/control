Source: hfd-service
Section: admin
Priority: optional
Build-Depends: cmake (>= 3.5),
               debhelper-compat (= 12),
               pkg-config,
               libandroid-properties-dev | hello,
               libdeviceinfo-dev,
               libhardware-dev | hello,
               android-headers | hello,
               libglib2.0-dev,
               libgbinder-dev | hello,
               libglibutil-dev | hello,
               libudev-dev,
               qtbase5-dev,
               qtfeedback5-dev,
               cmake-extras,
               qtdeclarative5-dev,
               systemd [linux-any],
               accountsservice:native,
               libaccountsservice-dev,
               libdbus-1-dev,
Maintainer: Marius Gripsgard <marius@ubports.com>
Standards-Version: 3.9.5
Homepage: https://gitlab.com/ubports/development/core/hfd-service
Vcs-Git: https://gitlab.com/ubports/development/core/hfd-service.git
Vcs-Browser: https://gitlab.com/ubports/development/core/hfd-service

Package: hfd-service
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: HFD service for Lomiri
  HFD service is a DBus activated service that manages human feedback devices
  such as leds and vibrators on mobile devices.

Package: libqt5feedback5-hfd
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         hfd-service,
Description: QtFeedback Qt plugin for HFD service
  HFD service is a DBus activated service that manages human feedback devices
  such as leds and vibrators on mobile devices.
  .
  This package provides a Qt plugin which integrates HFD service with the
  QtFeedback API.

Package: qml-module-hfd
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         hfd-service,
Description: QML plugin for HFD service
  HFD service is a DBus activated service that manages human feedback devices
  such as leds and vibrators on mobile devices.
  .
  This package provides a QML plugin which provides access to HFD service.

Package: hfd-service-tools
Section: admin
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         hfd-service,
Description: Tools for HFD service
  HFD service is a DBus activated service that manages human feedback devices
  such as leds and vibrators on mobile devices.
  .
  This package provides tools for testing HFD service.
